//
//  AllArticles.m
//  QuickEngadget
//
//  Created by Vanja Petkovic on 12/12/11.
//  Copyright (c) 2011 Vanja Petkovic All rights reserved.
//

#import "AllArticles.h"
#import "Article.h"
#import "HttpRequest.h"
#import "CXMLDocument.h"
#import "CXMLElement.h"
#import "CXMLNode.h"

static AllArticles *defaultArticles = nil;

@interface AllArticles()
-(NSString *) getImageUrl:(NSString *)description;
-(NSString *) addImageSizeToDesription:(NSString *) description;
@end

@implementation AllArticles

+ (AllArticles *)defaultArticles
{
    if (!defaultArticles) {
        
        //Create the Singleton
        defaultArticles = [[super allocWithZone:NULL] init];
    }
    return defaultArticles;
}

//Prevent creation of additional instances
+ (id)allocWithZone:(NSZone *)zone
{
    return [self defaultArticles];
}

- (id)init
{
    //If we already have an instance of PossessionStore then return the old instance
    if (defaultArticles) {
        return defaultArticles;
    }
    
    self = [super init];
    
    // instance of NSMutablearray
    if (self) {
        articles = [[NSMutableArray alloc] init];
        
    }
    
    return self;
}

- (void)setDelegate:(id)aDelegate
{
    delegate = aDelegate;
}

- (void) getArticles
{
    //get from url
    HttpRequest * request = [[HttpRequest alloc] initWithHttpDelegate:self];
    [request get:ENGADGET_URL];    
}

- (id)retain
{
    //Do Nothing
    return self;
}

- (NSUInteger)retainCount
{
    return NSUIntegerMax;
}

- (NSArray *)allArticles
{
    return articles;
}

- (void) didFinishDownload:(NSData *)data
{
    NSString* dataAsString = [[[NSString alloc] initWithData:data
                                                    encoding:NSUTF8StringEncoding] autorelease];

    CXMLDocument *rssParser = [[CXMLDocument alloc] initWithXMLString:dataAsString 
                                                               options:0
                                                                error:nil];
    
    NSArray *resultNodes = [rssParser nodesForXPath:@"//item" error:nil];
    
    [articles removeAllObjects];
    
    for (CXMLElement *resultElement in resultNodes) {
        Article *article = [[Article alloc] init];
        for(int counter = 0; counter < [resultElement childCount]; counter ++) {
			NSString *strName = [[resultElement childAtIndex:counter] name];
            NSString *strValue = [[resultElement childAtIndex:counter] stringValue];
            
            if([strName isEqualToString:@"title"])
                [article setArticleTitle:strValue];
            else if([strName isEqualToString:@"link"])
                [article setArticleUrl:strValue];
            else if([strName isEqualToString:@"pubDate"])
                [article setArticleDate:strValue];
            else if([strName isEqualToString:@"description"]) {
                [article setImageUrl:[self getImageUrl:strValue]];
                [article setArticleDescription:[self addImageSizeToDesription:strValue]];
            }
        }
        
        [articles addObject:article];
        [article release];
        article = nil;
    }
    
    
    [rssParser release];
    rssParser = nil;
    
    if ([delegate respondsToSelector:@selector(downloadDidFinishDownloadingArticles)])
        [delegate downloadDidFinishDownloadingArticles];

    
}

-(NSString *) addImageSizeToDesription:(NSString *) description
{
    NSString *part1, *part2;
    NSRange range = [description rangeOfString:@"<img"]; 
    
    int splitLocation = range.location;
    int splitLength = range.length;
    
    range.location = 0;
    range.length = splitLocation + splitLength;
    part1 = [description substringWithRange:range];
    
    range.location = range.length;
    range.length = [description length] - [part1 length];
    part2 = [description substringWithRange:range];
    
    NSString *str = @" width=300 height=300 align=center";
    
    NSString *result = @"";
    result = [result stringByAppendingString:part1];
    result = [result stringByAppendingString:str];
    result = [result stringByAppendingString:part2];
    
    NSLog(@"%@", result);
    return result;
    
    
}

- (NSString *) getImageUrl:(NSString *)description
{
    NSString *url;
    NSRange range = [description rangeOfString:@"<img"]; 
    
    range.location += range.length;
    range.length += 300;
    url = [description substringWithRange:range];
        
    range = [description rangeOfString:@"src=\""];
    range.location += range.length;
    range.length = 260;
    url = [description substringWithRange:range];
    
    NSRange endRange = [url rangeOfString:@"\""];
    
    range.location = 0;
    range.length = endRange.location;
    url = [url substringWithRange:range];
    
    return url;
}

@end
