//
//  Article.m
//  QuickEngadget
//
//  Created by Vanja Petkovic on 12/12/11.
//  Copyright (c) 2011 Vanja Petkovic All rights reserved.
//

#import "Article.h"

@interface Article()
@property (nonatomic, retain) NSMutableData *receivedData;
@end

@implementation Article

@synthesize articleTitle = articleTitle;
@synthesize articleUrl = articleUrl;
@synthesize articleDate = articleDate;
@synthesize articleDescription = articleDescription;
@synthesize imageUrl = imageUrl;
@synthesize image = image;
@synthesize delegate;

@synthesize receivedData;

- (void)dealloc {
    [articleTitle release];
    articleTitle = nil;
    [articleUrl release];
    articleUrl = nil;
    [articleDate release];
    articleDate = nil;
    [super dealloc];
}

- (UIImage *)image
{
    if (image == nil && !downloading)
    {
        if (imageUrl != nil && [imageUrl length] > 0)
        {
            NSURLRequest *req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:imageUrl]];
            NSURLConnection *con = [[NSURLConnection alloc]
                                    initWithRequest:req
                                    delegate:self
                                    startImmediately:NO];
            [con scheduleInRunLoop:[NSRunLoop currentRunLoop]
                           forMode:NSRunLoopCommonModes];
            [con start];
            
            
            
            if (con) 
            {
                NSMutableData *data = [[NSMutableData alloc] init];
                self.receivedData=data;
                [data release];
            } 
            else 
            {
                NSError *error = [NSError errorWithDomain:ArticleDownloadErrorDomain 
                                                     code:ArticleDownloadErrorNoConnection 
                                                 userInfo:nil];
                if ([self.delegate respondsToSelector:@selector(download:didFailWithError:)])
                    [delegate download:self didFailWithError:error];
            }   
            [req release];
            
            downloading = YES;
        }
    }
    return image;
}

#pragma mark -
#pragma mark NSURLConnection Callbacks
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response 
{
    [receivedData setLength:0];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data 
{
    [receivedData appendData:data];
}
- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error 
{
    [connection release];
    if ([delegate respondsToSelector:@selector(download:didFailWithError:)])
        [delegate download:self didFailWithError:error];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection 
{
    self.image = [UIImage imageWithData:receivedData];
    if ([delegate respondsToSelector:@selector(downloadDidFinishDownloading:)])
        [delegate downloadDidFinishDownloading:self];
    
    [connection release];
    self.receivedData = nil;
}

#pragma mark -
#pragma mark Comparison
- (NSComparisonResult)compare:(id)theOther
{
    Article *other = (Article *)theOther;
    return [self.articleTitle compare:other.articleTitle];
}

@end
