//
//  ArchiveArticle.h
//  QuickEngadget
//
//  Created by Vanja Petkovic on 1/24/12.
//  Copyright (c) 2012 Vanja Petkovic All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ArchiveArticle : NSManagedObject

@property (nonatomic, retain) NSString * articleTitle;
@property (nonatomic, retain) NSString * articleUrl;
@property (nonatomic, retain) NSString * articleDate;
@property (nonatomic, retain) NSString * articleDescription;
@property (nonatomic, retain) NSString * imageUrl;

@end
