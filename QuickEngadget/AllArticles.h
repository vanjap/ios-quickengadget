//
//  AllArticles.h
//  QuickEngadget
//
//  Created by Vanja Petkovic on 12/12/11.
//  Copyright (c) 2011 Vanja Petkovic All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HttpDelegate.h"

#define ENGADGET_URL @"http://www.engadget.com/rss.xml"

#define NOTIFICATION_MODEL_START_UPDATE @"START UPDATE NOTIFICATION"

@class AllArticles;
@protocol AllArticlesDownloadDelegate
- (void)downloadDidFinishDownloadingArticles;
@end


@interface AllArticles : NSObject <HttpDelegate>
{
    NSMutableArray *articles;
    id <NSObject, AllArticlesDownloadDelegate> delegate;
}

//class method
+ (AllArticles *)defaultArticles;

- (NSArray *)allArticles;
- (void)setDelegate:(id)aDelegate;
- (void) getArticles;


@end
