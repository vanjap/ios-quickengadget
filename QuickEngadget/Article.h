//
//  Article.h
//  QuickEngadget
//
//  Created by Vanja Petkovic on 12/12/11.
//  Copyright (c) 2011 Vanja Petkovic All rights reserved.
//

#import <Foundation/Foundation.h>

#define ArticleDownloadErrorDomain @"Article Download Error Domain"
enum 
{   ArticleDownloadErrorNoConnection = 1000,
};

@class Article;
@protocol ArticleImageDownloadDelegate
- (void)downloadDidFinishDownloading:(Article *)download;
- (void)download:(Article *)download didFailWithError:(NSError *)error;
@end


@interface Article : NSObject {
@private    NSMutableData *receivedData;
            BOOL downloading;
}

@property (copy) NSString *articleTitle;
@property (copy) NSString *articleUrl;
@property (copy) NSString *articleDate;
@property (copy) NSString *articleDescription;
@property (copy) NSString *imageUrl;
@property (nonatomic, retain) UIImage *image;
@property (nonatomic, assign) id <NSObject, ArticleImageDownloadDelegate> delegate;

@end
