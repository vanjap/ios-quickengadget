//
//  ArchivedArticlesController.h
//  QuickEngadget
//
//  Created by Vanja Petkovic on 2/4/12.
//  Copyright (c) 2012 Vanja Petkovic All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArchivedArticlesController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UINavigationController *navigationControler;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
