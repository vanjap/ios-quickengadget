//
//  DetailArticleController.h
//  QuickEngadget
//
//  Created by Vanja Petkovic on 12/14/11.
//  Copyright (c) 2011 Vanja Petkovic All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Article.h"
#import "ArchiveArticle.h"

@interface DetailArticleController : UIViewController 
{
    Article *article;
    ArchiveArticle *archiveArticle;
}

@property (retain, nonatomic) IBOutlet UIWebView *webView;

- (void)setArticle:(Article *)aArticle;
- (void)setArchiveArticle:(ArchiveArticle *)article;

@end
