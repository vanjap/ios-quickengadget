//
//  ArticlesController.h
//  QuickEngadget
//
//  Created by Vanja Petkovic on 12/12/11.
//  Copyright (c) 2011 Vanja Petkovic All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlowCoverView.h"
#import "Article.h"
#import "AllArticles.h"

#define ROW_HEIGHT 80.0

@interface ArticlesController : UIViewController <UITableViewDataSource, UITableViewDelegate, 
    ArticleImageDownloadDelegate, AllArticlesDownloadDelegate, FlowCoverViewDelegate>

@property (strong, nonatomic) IBOutlet UINavigationController *navigationControler;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet FlowCoverView *flowCoverView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *publishDateLabel;

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;


-(void) updateModel;

@end
