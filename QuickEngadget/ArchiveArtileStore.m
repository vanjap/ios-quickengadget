//
//  ArchiveArtileStore.m
//  QuickEngadget
//
//  Created by Vanja Petkovic on 2/4/12.
//  Copyright (c) 2012 Vanja Petkovic. All rights reserved.
//

#import "ArchiveArtileStore.h"
#import "FileHelpers.h"

@interface ArchiveArtileStore()
- (void)fetchArchiveArticlesIfNecessary;
@end

static ArchiveArtileStore *defaultStore = nil;

@implementation ArchiveArtileStore 
+ (ArchiveArtileStore *)defaultStore
{
    if (!defaultStore) {
        // Create the singleton
        defaultStore = [[super allocWithZone:NULL] init];
    }
    return defaultStore;
}

// Prevent creation of additional instances
+ (id)allocWithZone:(NSZone *)zone
{
    return [self defaultStore];
}


- (id)init {
    // If we already have an instance of LocationStore...
    if (defaultStore) {
        return defaultStore;
    }   
    
    self = [super init];
    
    // Read in LocationReminder.xcdatamodeld
    model = [[NSManagedObjectModel mergedModelFromBundles:nil] retain];
    // NSLog(@"model = %@", model);
    
    NSPersistentStoreCoordinator *psc =
    [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
    
    // Where does the SQLite file go?
    NSString *path = pathInDocumentDirectory(@"store.data");
    NSURL *storeURL = [NSURL fileURLWithPath:path];
    NSError *error = nil;
    
    if (![psc addPersistentStoreWithType:NSSQLiteStoreType
                           configuration:nil
                                     URL:storeURL
                                 options:nil
                                   error:&error]) {
        [NSException raise:@"Open failed"
                    format:@"Reason: %@", [error localizedDescription]];
    }
    
    // Create the managed object context
    context = [[NSManagedObjectContext alloc] init];
    [context setPersistentStoreCoordinator:psc];
    
    [psc release];
    // The managed object context can manage undo, but we don't need it
    [context setUndoManager:nil];
    
    return self;
}

- (BOOL)saveChanges
{
    NSError *err = nil;
    BOOL successful = [context save:&err];
    
    if(!successful) {
        NSLog(@"Error saving %@", [err localizedDescription]);
    }
    
    return successful;                   
}

- (void)fetchArchiveArticlesIfNecessary
{
    if (!allArchiveArticles) {
        NSFetchRequest *request = [[[NSFetchRequest alloc] init] autorelease];
        
        NSEntityDescription *e = [[model entitiesByName] objectForKey:@"ArchiveArticle"];
        [request setEntity:e];
        
        NSSortDescriptor *sd = [NSSortDescriptor 
                                sortDescriptorWithKey:@"articleTitle"
                                ascending:YES];
        [request setSortDescriptors:[NSArray arrayWithObject:sd]];
        
        NSError *error;
        NSArray *result = [context executeFetchRequest:request error:&error];
        if (!result) {
            [NSException raise:@"Fetch failed"
                        format:@"Reason: %@", [error localizedDescription]];
        }
        
        allArchiveArticles = [[NSMutableArray alloc] initWithArray:result];
    }
}

- (NSArray *)allArchiveArticles
{
    [self fetchArchiveArticlesIfNecessary];
    return allArchiveArticles;
    
}

- (ArchiveArticle *)addArchivedArticleWithDataFromArticle:(Article *)article
{
    [self fetchArchiveArticlesIfNecessary];
    
    ArchiveArticle *a = [NSEntityDescription insertNewObjectForEntityForName:@"ArchiveArticle"
                                                inManagedObjectContext:context];
    
    [a setArticleTitle:[article articleTitle]];
    [a setArticleUrl:[article articleUrl]];
    [a setArticleDate:[article articleDate]];
    [a setArticleDescription:[article articleDescription]];
    [a setImageUrl:[article imageUrl]];
    
    [allArchiveArticles addObject:a];
    return a;    
}

- (void)removeArchivedArticle:(ArchiveArticle *)article
{
    [self fetchArchiveArticlesIfNecessary];
    
    [context deleteObject:article];
    [allArchiveArticles removeObjectIdenticalTo:article];  
}

@end
