//
//  ArticlesController.m
//  QuickEngadget
//
//  Created by Vanja Petkovic on 12/12/11.
//  Copyright (c) 2011 Vanja Petkovic All rights reserved.
//

#import "ArticlesController.h"
#import "Article.h"
#import "AllArticles.h"
#import "DetailArticleController.h"
#import "ArticleCell.h"
#import "Utils.h"
#import "ArchiveArtileStore.h"

//private
@interface ArticlesController()
- (void)openDetailView:(int)index;
- (void)setViewsVisibility;
- (void)setLabelsForFirstEntry;
@end

@implementation ArticlesController

@synthesize navigationControler;
@synthesize tableView;
@synthesize flowCoverView;
@synthesize titleLabel;
@synthesize publishDateLabel;
@synthesize activityIndicator;

- (void)viewDidLoad
{
    self.navigationItem.title = @"QuickEngadget";
    
    //[flowCoverView setViewDelegate:self];
    [self setViewsVisibility];
    
    //activity bar
    activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    [self navigationItem].leftBarButtonItem = barButton;
    [activityIndicator startAnimating];
    
    [self updateModel];
    
    //listen for notification when application is returning back to foreground
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(updateModel)
                                                 name:NOTIFICATION_MODEL_START_UPDATE
                                               object:nil];
    
    //listen for rotatiton
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRotate:) name:@"UIDeviceOrientationDidChangeNotification" object:nil];
    
}

- (void) didRotate:(NSNotification *)notification 
{     
    [self setViewsVisibility];
}

- (void)setViewsVisibility 
{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    if (UIInterfaceOrientationIsLandscape(orientation)) {
        tableView.hidden = YES;
        
        [flowCoverView emptyDataCache];
        flowCoverView.hidden = NO;
        [flowCoverView draw];
        
        titleLabel.hidden = NO;
        publishDateLabel.hidden = NO;
        
        
        [self setLabelsForFirstEntry];
        
    } else if (UIInterfaceOrientationIsPortrait(orientation)) {
        tableView.hidden = NO;
        flowCoverView.hidden = YES;
        titleLabel.hidden = YES;
        publishDateLabel.hidden = YES;
        [flowCoverView setNeedsDisplay];
    }
    
}

- (void)setLabelsForFirstEntry
{
    if([titleLabel.text isEqualToString:@"Label"] || 
       [publishDateLabel.text isEqualToString:@"Label"]) {
        
        if([[[AllArticles defaultArticles] allArticles] count] > 0) {
            Article *entry = [[[AllArticles defaultArticles] allArticles] objectAtIndex:0];
            titleLabel.text = entry.articleTitle;
            publishDateLabel.text = entry.articleDate;
        }
        
    }
}


- (void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)dealloc
{
    [navigationControler release];
    [tableView release];
    [super dealloc];
}

- (void)updateModel
{
    [activityIndicator startAnimating];

    //Load articles
    AllArticles *articles = [AllArticles defaultArticles];
    [articles setDelegate:self];
    [articles getArticles];    
}


//- (void)modelUpdated
- (void)downloadDidFinishDownloadingArticles
{
    [activityIndicator stopAnimating];
    [tableView reloadData];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)x
{
    return YES;
}

- (NSInteger)tableView:(UITableView *)table
 numberOfRowsInSection:(NSInteger)section 
{
    
    return [[[AllArticles defaultArticles] allArticles] count];
}


- (CGFloat)tableView:(UITableView *)tableView 
heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return ROW_HEIGHT;
}

// Modify tableView:cellForRowAtIndexPath below like the following
- (UITableViewCell *)tableView:(UITableView *)aTableView 
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"ArticleCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *a = [[NSBundle mainBundle] loadNibNamed:@"ArticleCellView" owner:nil options:nil];
        cell = [a objectAtIndex:0];
    }
    
    Article *entry = [[[AllArticles defaultArticles] allArticles] objectAtIndex:[indexPath row]];
    if(entry.delegate == nil)
        entry.delegate = self;
    
    
    ArticleCell *ac = (ArticleCell *)cell; 
    
    ac.articleTitle.text = entry.articleTitle;
    ac.publishDate.text = entry.articleDate;

    [ac.image setImage:entry.image];

    return ac;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell 
                                         forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row % 2 == 0) {
        cell.backgroundColor = [UIColor colorWithRed:226.0/255.0 
                                               green:231.0/255.0 
                                                blue:238.0/255.0 alpha:1];
    } else {
        cell.backgroundColor = [UIColor whiteColor];
    }  
}

- (void)tableView:(UITableView *)aTableView 
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self openDetailView:[indexPath row]];    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

//archiving
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(editingStyle == UITableViewCellEditingStyleDelete) {
        ArchiveArtileStore *store = [ArchiveArtileStore defaultStore];
        
        Article *article = [[[AllArticles defaultArticles] allArticles] 
                            objectAtIndex:[indexPath row]];
        
        [store addArchivedArticleWithDataFromArticle:article];
        [store saveChanges];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"Archive";
}

- (void) openDetailView:(int)index
{
    DetailArticleController *detailView = [[[DetailArticleController alloc] init] autorelease];
    Article *article = [[[AllArticles defaultArticles] allArticles] objectAtIndex:index];
    
    //Give detail view a pointer to the possession object in row
    [detailView setArticle:article];
    
    //Push to navigation controller stack
    [[self navigationController] pushViewController:detailView animated:YES];
    
    
}

#pragma mark Article Image Download Delegate Methods
- (void)downloadDidFinishDownloading:(Article *)download
{
    [flowCoverView draw];
        
    NSUInteger index = [[[AllArticles defaultArticles] allArticles] indexOfObject:download]; 
    NSUInteger indices[] = {0, index};
    NSIndexPath *path = [[NSIndexPath alloc] initWithIndexes:indices length:2];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:path] withRowAnimation:UITableViewRowAnimationNone];
    [path release];

    download.delegate = nil;
}
- (void)download:(Article *)download didFailWithError:(NSError *)error
{
    NSLog(@"Error: %@", [error localizedDescription]);
}
											
#pragma mark FlowCover Callbacks
- (int)flowCoverNumberImages:(FlowCoverView *)view
{
	return [[[AllArticles defaultArticles] allArticles] count];
}

- (UIImage *)flowCover:(FlowCoverView *)view cover:(int)image
{
	
    Article *entry = [[[AllArticles defaultArticles] allArticles] objectAtIndex:image];
    return entry.image;
}

- (void)flowCover:(FlowCoverView *)view didSelect:(int)image
{
	[self openDetailView:image];
}

- (void)flowCover:(FlowCoverView *)view highlighted:(int)cover
{
    Article *entry = [[[AllArticles defaultArticles] allArticles] objectAtIndex:cover];
    titleLabel.text = entry.articleTitle;
    publishDateLabel.text = entry.articleDate;
    
}

@end
