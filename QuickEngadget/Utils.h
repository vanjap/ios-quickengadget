//
//  Utils.h
//  QuickEngadget
//
//  Created by Vanja Petkovic on 12/16/11.
//  Copyright (c) 2011 Vanja Petkovic All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Utils : NSObject

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize; 

@end
