//
//  HttpRequest.m
//  QuickEngadget
//
//  Created by Vanja Petkovic on 12/12/11.
//  Copyright (c) 2011 Vanja Petkovic All rights reserved.
//

#import "HttpRequest.h"

@implementation HttpRequest

@synthesize receivedData;

- init 
{
    if ((self = [super init])) {
 		
    }
    return self;
}

- (id)initWithHttpDelegate: (id)aDelegate
{
    [self init];
    
    if(self) {
        self.delegate = aDelegate;
    }
     
    return self;
}

- (void)dealloc 
{
    [super dealloc];
}


- (void)setDelegate:(id)val
{
    delegate = val;
}

- (id)delegate
{
    return delegate;
}

- (void)get: (NSString *)urlString {
 	
 	NSLog ( @"GET: %@", urlString );
    
 	self.receivedData = [[NSMutableData alloc] init];
 	
    NSURLRequest *request = [[NSURLRequest alloc]
 							 initWithURL: [NSURL URLWithString:urlString]
 							 cachePolicy: NSURLRequestReloadIgnoringLocalCacheData
 							 timeoutInterval: 10
 							 ];
    
    NSURLConnection *connection = [[NSURLConnection alloc]
 								   initWithRequest:request
 								   delegate:self
 								   startImmediately:YES];
 	if(!connection) {
 		NSLog(@"connection failed :(");
 	} else {
 		NSLog(@"connection succeeded  :)");
 		
 	}
 	
 	[connection release];
    [request release];  
    [receivedData release];  
}

// ====================
// Callbacks
// ====================

#pragma mark NSURLConnection delegate methods
- (NSURLRequest *)connection:(NSURLConnection *)connection
 			 willSendRequest:(NSURLRequest *)request
 			redirectResponse:(NSURLResponse *)redirectResponse {
 	NSLog(@"Connection received data, retain count");
    return request;
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
 	NSLog(@"Received response: %@", response);
 	
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
 	NSLog(@"Received %d bytes of data", [data length]); 
 	
    [receivedData appendData:data];
 	NSLog(@"Received data is now %d bytes", [receivedData length]); 
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
 	NSLog(@"Error receiving response: %@", error);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection 
{
 	if ([delegate respondsToSelector:@selector(didFinishDownload:)]) {
 		[delegate didFinishDownload:receivedData];
 	}
}


@end
