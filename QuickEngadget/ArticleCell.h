//
//  ArticleCell.h
//  QuickEngadget
//
//  Created by Vanja Petkovic on 12/15/11.
//  Copyright (c) 2011 Vanja Petkovic All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *articleTitle;
@property (strong, nonatomic) IBOutlet UILabel *publishDate;
@property (strong, nonatomic) IBOutlet UIImageView *image;

@end
