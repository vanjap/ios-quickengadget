//
//  ArticleCell.m
//  QuickEngadget
//
//  Created by Vanja Petkovic on 12/15/11.
//  Copyright (c) 2011 Vanja Petkovic All rights reserved.
//

#import "ArticleCell.h"

@implementation ArticleCell

@synthesize articleTitle;
@synthesize publishDate;
@synthesize image;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
