//
//  ArchivedArticlesController.m
//  QuickEngadget
//
//  Created by Vanja Petkovic on 2/4/12.
//  Copyright (c) 2012 Vanja Petkovic All rights reserved.
//

#import "ArchivedArticlesController.h"
#import "DetailArticleController.h"
#import "ArchiveArtileStore.h"
#import "ArchiveArticle.h"

@interface ArchivedArticlesController()
- (void)openDetailView:(int)index;
@end

@implementation ArchivedArticlesController

@synthesize navigationControler;
@synthesize tableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"Archive";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

#pragma table view
- (NSInteger)tableView:(UITableView *)table
 numberOfRowsInSection:(NSInteger)section 
{
    return [[[ArchiveArtileStore defaultStore] allArchiveArticles] count];
}

- (UITableViewCell *)tableView:(UITableView *)aTableView 
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell.
    ArchiveArticle *a = [[[ArchiveArtileStore defaultStore] allArchiveArticles] objectAtIndex:[indexPath row]];
    cell.textLabel.text = [a articleTitle];
    
    return cell;    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(editingStyle == UITableViewCellEditingStyleDelete) {
        ArchiveArtileStore *store = [ArchiveArtileStore defaultStore];
        
        ArchiveArticle *article = [[store allArchiveArticles] objectAtIndex:[indexPath row]];
        
        [store removeArchivedArticle:article];
        [store saveChanges];

        NSArray * array = [[NSArray alloc] initWithObjects:indexPath, nil];
        
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:array 
                              withRowAnimation:UITableViewRowAnimationRight];
        [self.tableView endUpdates];
        
        [array release];
        
    }
}

- (void)tableView:(UITableView *)aTableView 
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self openDetailView:[indexPath row]];    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma
- (void) openDetailView:(int)index
{
    DetailArticleController *detailView = [[[DetailArticleController alloc] init] autorelease];
    
    ArchiveArticle *a = [[[ArchiveArtileStore defaultStore] allArchiveArticles] objectAtIndex:index];
    
    //Give detail view a pointer to the possession object in row
    [detailView setArchiveArticle:a];
    
    //Push to navigation controller stack
    [[self navigationController] pushViewController:detailView animated:YES];
    
    
}

@end
