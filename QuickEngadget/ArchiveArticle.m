//
//  ArchiveArticle.m
//  QuickEngadget
//
//  Created by Vanja Petkovic on 1/24/12.
//  Copyright (c) 2012 Vanja Petkovic All rights reserved.
//

#import "ArchiveArticle.h"


@implementation ArchiveArticle

@dynamic articleTitle;
@dynamic articleUrl;
@dynamic articleDate;
@dynamic articleDescription;
@dynamic imageUrl;

@end
