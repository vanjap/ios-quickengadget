//
//  ArchiveArtileStore.h
//  QuickEngadget
//
//  Created by Vanja Petkovic on 2/4/12.
//  Copyright (c) 2012 Vanja Petkovic All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ArchiveArticle.h"
#import "Article.h"

@interface ArchiveArtileStore : NSObject
{
    NSMutableArray *allArchiveArticles;
    
    NSManagedObjectContext *context;
    NSManagedObjectModel *model;

}

+ (ArchiveArtileStore *)defaultStore;

#pragma Archive Articles
- (NSArray *)allArchiveArticles;
- (ArchiveArticle *)addArchivedArticleWithDataFromArticle:(Article *)article;
- (void)removeArchivedArticle:(ArchiveArticle *)article;
- (BOOL)saveChanges;

@end
