//
//  DetailArticleController.m
//  QuickEngadget
//
//  Created by Vanja Petkovic on 12/14/11.
//  Copyright (c) 2011 Vanja Petkovic All rights reserved.
//

#import "DetailArticleController.h"

/*@interface DetailArticleController()
- (void)createTwitterButton;
- (IBAction)twitterButtonClicked:(id)sender;
@end*/

@implementation DetailArticleController

@synthesize webView = _webView;

- (void)viewDidLoad
{
    if(article) {
        [self.webView loadHTMLString:[article articleDescription] 
                             baseURL:nil];    
    }
    else {
        [self.webView loadHTMLString:[archiveArticle articleDescription] 
                             baseURL:nil];    
        
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void) setArticle:(Article *)aArticle
{
    article = aArticle;
    archiveArticle = nil;
}

- (void)setArchiveArticle:(ArchiveArticle *)aArticle
{
    article = nil;
    archiveArticle = aArticle;
}


@end
