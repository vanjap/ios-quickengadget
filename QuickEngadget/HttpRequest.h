//
//  HttpRequest.h
//  QuickEngadget
//
//  Created by Vanja Petkovic on 12/12/11.
//  Copyright (c) 2011 Vanja Petkovic All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HttpDelegate.h"

@interface HttpRequest : NSObject
{
    id delegate;
    NSMutableData *receivedData;
    NSURL *url;
}

@property (nonatomic,retain) NSMutableData *receivedData;
@property (retain) id delegate;

- (id)initWithHttpDelegate: (id)delegate;
- (void)get: (NSString *)urlString;



@end
