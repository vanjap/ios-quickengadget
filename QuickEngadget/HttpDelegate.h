//
//  HttpDelegate.h
//  QuickEngadget
//
//  Created by Vanja Petkovic on 12/12/11.
//  Copyright (c) 2011 Vanja Petkovic All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HttpDelegate <NSObject>

@required
- (void) didFinishDownload:(NSData *)data;

@end
