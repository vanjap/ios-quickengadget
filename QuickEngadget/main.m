//
//  main.m
//  QuickEngadget
//
//  Created by Vanja Petkovic on 12/12/11.
//  Copyright (c) 2011 Vanja Petkovic All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
